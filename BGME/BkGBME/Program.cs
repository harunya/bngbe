﻿using System;
using System.IO;
using BkGBME.Opcodes;

namespace BkGBME
{
    class Program
    {
        public static int EntryPointAddress { get; } = 0x150;
        private static int _pc = 0;
        private static byte[] _bytes;
        
        static void Main(string[] args)
        {
            _bytes = File.ReadAllBytes("Resources/cpu_instrs.gb");

            for (_pc = EntryPointAddress; _pc < _bytes.Length; ++_pc)
            {
                Console.WriteLine($"{_pc:x8}| {BaseOpcode.Find(_bytes[_pc])}");
            }
        }
    }
}