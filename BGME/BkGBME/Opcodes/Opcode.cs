﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BkGBME.Opcodes
{
    public abstract class BaseOpcode
    {
        public static int Arguments { get; } = 0;
        
        
        public static void Execute(byte[] args)
        {
            
        }
        
        public static bool IsMatch(byte b)
        {
            return false;
        }

        public new static string ToString() => MethodBase.GetCurrentMethod()?.DeclaringType?.Name;

        public static BaseOpcode Find(byte b)
        {
            var exporters = typeof(BaseOpcode)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(BaseOpcode)) && !t.IsAbstract &&
                            (bool) t.GetMethod("IsMatch")?.Invoke(t, new object[] {b}))
                .ToArray();

            return exporters.ToArray().Length == 0 ? new Undefined() : (BaseOpcode)Activator.CreateInstance(exporters[0]);
        }
    }
}