﻿namespace BkGBME.Opcodes
{
    public class Undefined: BaseOpcode
    {
        public static int Arguments { get; private set; } = 0;
        
        public new static void Execute(byte[] args)
        {
            
        }
        public new static bool IsMatch(byte b) => false;
    }
}