﻿using BGME.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BGME.Opcodes
{
    public static class OpcodeUtils
    {
        public static string[] Reg = new string[] { "B", "C", "D", "E", "H", "L", "(HL)", "A" };
        public static string[] Reg16 = new string[] { "BC", "?", "DE", "?", "HL", "", "SP" };
        public static string[] Flags = new string[] { "Z", "N", "H", "C" };

        public static int GetInt(byte lsb, byte msb) => ((int)(lsb + (msb << 8)));
        public static bool GetBit(byte b, int bitNumber) => ((b & (1 << bitNumber - 1)) != 0);

        public static FlagValue IsHalfcarryFlag(byte a, byte b) => (((a & 0xf) + (b & 0xf)) & 0x10) == 0x10 ? FlagValue.Set : FlagValue.Reset;
        public static FlagValue IsCarryFlag(byte a, byte b) => (((a & 0xff) + (b & 0xff)) & 0x10) == 0x10 ? FlagValue.Set : FlagValue.Reset;
        public static FlagValue IsNullFlag(byte a) => a == 0 ? FlagValue.Set : FlagValue.Reset;
        public static FlagValue IsBoolFlag(bool a) => a ? FlagValue.Set : FlagValue.Reset;
    }
    public interface IOpcode
    {
        public Regex Mask { get; }
        public string Title { get; }
        public int ArgsCount { get; }
        public byte LastValue { get; set; }

        public void Execute(Instance inst, params byte[] args)
        {

        }

        public void Eval(Instance emu, byte[] opargs) => Execute(emu, GetArguments(this, opargs));
        public string GetOpDesc(byte[] opargs) => ToString(GetArguments(this, opargs));

        public virtual string ToString(params byte[] args)
        {
            return $"{Title} {string.Join("", GetArguments(this, args))}";
        }

        public static byte[] GetArguments(IOpcode opcode, byte[] args)
        {
            var res = new List<byte>();

            var s = Convert.ToString(opcode.LastValue, 2).PadLeft(8, '0');

            int i = 0;
            foreach (Group a in opcode.Mask.Match(s).Groups)
            {
                if (i++ == 0)
                    continue;

                res.Add(Convert.ToByte(a.Value, 2));
            }

            foreach(var a in args)
            {
                res.Add(a);
            }


            return res.ToArray();
        }
    }
}
