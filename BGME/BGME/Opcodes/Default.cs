﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BGME.Opcodes.Default
{
    

    class NOP : IOpcode //nop
    {
        public Regex Mask => new Regex("00000000");
        public string Title => "NOP";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }
    }
    class STOP : IOpcode //stop 0
    {
        public Regex Mask => new Regex("00010000");
        public string Title => "STOP";
        public int ArgsCount => 1;
        public byte LastValue { get; set; }
    }
    class LD8B : IOpcode //ld A, d8
    {
        public Regex Mask => new Regex("00(.{3})110");
        public string Title => "LD";
        public int ArgsCount => 1;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg[args[0]]} ${Convert.ToString(args[1], 16)}";
        }

    }
    class LD8RB : IOpcode //ld A, B
    {
        public Regex Mask => new Regex("01(.{3})(.{3})");
        public string Title => "LD";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] argss)
        {
            var args = "";
            foreach (var i in IOpcode.GetArguments(this, argss))
            {
                args += $"{OpcodeUtils.Reg[i]} ";
            }

            return $"{Title} {args}";
        }
    }

    class LD16B : IOpcode //ld A, d16
    {
        public Regex Mask => new Regex("00(.{3})001");
        public string Title => "LD";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg16[args[0]]} {args[1]}";
        }

    }

    class OR : IOpcode //or A
    {
        public Regex Mask => new Regex("10110(.{3})");
        public string Title => "OR";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg[args[0]]}";
        }

    }

    class INC16 : IOpcode //inc BC
    {
        public Regex Mask => new Regex("00(.{3})011");
        public string Title => "INC";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg16[args[0]]}";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            var nam = OpcodeUtils.Reg16[args[0]];
            inst.Registers.SetReg16Value(nam, (byte)(inst.Registers.GetReg16Value(nam) + 1));
        }

    }

    class INC : IOpcode //inc B
    {
        public Regex Mask => new Regex("00(.{3})100");
        public string Title => "INC";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg[args[0]]}";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            if (OpcodeUtils.Reg[args[0]] == "(HL)")
            {
                var addr = inst.Registers.GetReg16Value("HL");
                var data = inst.GetBytesFromMemory(inst.Registers.GetReg16Value("HL"), 1)[0];
                inst.CopyBytesToMemory(addr, new byte[] { data });
                return;
            }
            inst.Registers.SetRegValue(args[0], (byte)(inst.Registers.GetRegValue(args[0]) + 1));
        }

    }

    class DEC : IOpcode //dec B
    {
        public Regex Mask => new Regex("00(.{3})101");
        public string Title => "DEC";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args) => $"{Title} {OpcodeUtils.Reg[args[0]]}";
        public void Execute(Instance inst, params byte[] args)
        {
            if (OpcodeUtils.Reg[args[0]] == "(HL)")
            {
                var addr = inst.Registers.GetReg16Value("HL");
                var data = inst.GetBytesFromMemory(inst.Registers.GetReg16Value("HL"), 1)[0];
                inst.CopyBytesToMemory(addr, new byte[] { data });
                return;
            }

            inst.Registers.SetRegValue(args[0], (byte)(inst.Registers.GetRegValue(args[0]) - 1));
        }

    }

    class CALL : IOpcode //CALL a16
    {
        public Regex Mask => new Regex("11001101");
        public string Title => "CALL";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} ${Convert.ToString(OpcodeUtils.GetInt(args[0], args[1]), 16)}";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            // TODO: CALL
        }

    }

    class LDHa8A : IOpcode //LDH (a8),A
    {
        public Regex Mask => new Regex("11100000");
        public string Title => "LDH";
        public int ArgsCount => 1;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} (${Convert.ToString(args[0], 16)}) A";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(args[0], 0xFF), BitConverter.GetBytes(inst.Registers.A.Value));

        }

    }
    class LDHAa8 : IOpcode //LDH A, (a8)
    {
        public Regex Mask => new Regex("11110000");
        public string Title => "LDH";
        public int ArgsCount => 1;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} A (${Convert.ToString(args[0], 16)})";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(inst.Registers.A.Value, 0xFF), inst.GetBytesFromMemory(args[0], 8));

        }
    }

    class LDHa16A : IOpcode //LD (a16), A
    {
        public Regex Mask => new Regex("11101010");
        public string Title => "LD";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} (${Convert.ToString(OpcodeUtils.GetInt(args[0], args[1]), 16)}) A";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(args[0], args[1]), BitConverter.GetBytes(inst.Registers.A.Value));

        }

    }

    class LDa8A : IOpcode //LDH (a8), A
    {
        public Regex Mask => new Regex("11111010");
        public string Title => "LD";
        public int ArgsCount => 1;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} A (${Convert.ToString(OpcodeUtils.GetInt(args[0], args[1]), 16)})";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(args[0], 0xFF), BitConverter.GetBytes(inst.Registers.A.Value));

        }


    }
    class LDa16SP : IOpcode //LD (a16) SP
    {
        public Regex Mask => new Regex("00001000");
        public string Title => "LD";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} (${Convert.ToString(OpcodeUtils.GetInt(args[0], args[1]), 16)}) SP";
        }
        public void Execute(Instance inst, params byte[] args)
        {
            // TODO: SP pointer insert
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(args[0], args[1]), new byte[] { 0 });

        }


    }
    class ADDC : IOpcode //ADDC A, B
    {
        public Regex Mask => new Regex("10001(.{3})");
        public string Title => "ADDC";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args) => $"{Title} A {OpcodeUtils.Reg[args[0]]}";


        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = inst.Registers.GetRegValue(args[0]);
            var result = (byte)(recentValue + value + (inst.Registers.GetFlag(Flag.C) ? 1 : 0));


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);

        }

    }
    class ADD : IOpcode //ADD A, B
    {
        public Regex Mask => new Regex("10000(.{3})");
        public string Title => "ADD";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} A {OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = inst.Registers.GetRegValue(args[0]);
            var result = (byte)(recentValue + value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);

        }


    }
    class CALLC : IOpcode //CALL Z, a16
    {
        public Regex Mask => new Regex("11(001|011)100");
        public string Title => "CALL";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            // TODO: CALLC

            return $"{Title} {new[] { "?", "Z", "??", "C" }[args[0]]} ${Convert.ToString(OpcodeUtils.GetInt(args[1], args[2]), 16)}";
        }

    }

    class JPa16 : IOpcode //JP a16
    {
        public Regex Mask => new Regex("11000011");
        public string Title => "JP";
        public int ArgsCount => 2;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} ${Convert.ToString(OpcodeUtils.GetInt(args[0], args[1]), 16)}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            inst.Registers.PC = OpcodeUtils.GetInt(args[0], args[1]);
        }

    }
    

    class AND : IOpcode //AND a
    {
        public Regex Mask => new Regex("10100(.{3})");
        public string Title => "AND";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            return $"{Title} {OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var result = (byte)(recentValue & inst.Registers.GetRegValue(args[0]));


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                FlagValue.Set,
                FlagValue.Reset
            );
            inst.Registers.A.SetValue(result);
        }

    }
    

    class SUB : IOpcode //SUB B
    {
        public Regex Mask => new Regex("10010(.{3})");
        public string Title => "SUB";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = inst.Registers.GetRegValue(args[0]);
            var result = (byte)(recentValue - value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Set,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);
        }

    }
    
    class RET : IOpcode //RET Z
    {
        public Regex Mask => new Regex("11(001|011)000");
        public string Title => "RET";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            

            return $"{Title} {OpcodeUtils.Flags[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            // TODO: RET

            //inst.Registers.PC = OpcodeUtils.GetInt(args[0], args[1]);
        }

    }
    
    class PUSH : IOpcode //PUSH Z
    {
        public Regex Mask => new Regex("11(001|010|100|110)101");
        public string Title => "PUSH";
        public int ArgsCount => 0;
        public byte LastValue { get; set; }

        public string ToString(params byte[] args)
        {
            
            var f = new string[] { "BC", "?", "DE", "?", "HL", "?", "AF" };

            return $"{Title} {f[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            // TODO: PUSH

            //inst.Registers.PC = OpcodeUtils.GetInt(args[0], args[1]);
        }

    }

    class RETI : IOpcode //RETI
    {
        public Regex Mask => new Regex("11011001");
        public string Title => "RETI";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;
        public void Execute(Instance inst, params byte[] args)
        {
            // TODO: RETI

            //inst.Registers.PC = OpcodeUtils.GetInt(args[0], args[1]);
        }

    }
    class SBC : IOpcode //SBC A, B
    {
        public Regex Mask => new Regex("10011(.{3})");
        public string Title => "SBC";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;

        public string ToString(params byte[] args)
        {
            

            return $"{Title} A {OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue - value - (inst.Registers.GetFlag(Flag.C) ? 1 : 0));


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Set,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);
        }

    }

    class CP : IOpcode //CP n
    {
        public Regex Mask => new Regex("10111(.{3})");
        public string Title => "CP";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} {OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var value = args[0];
            var result = (byte)(inst.Registers.A.Value - value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsBoolFlag(result == 0),
                FlagValue.Set,
                OpcodeUtils.IsBoolFlag(result > 0), // TODO: Set if no borrow from bit 4
                OpcodeUtils.IsBoolFlag(result < 0)
            );

        }
    }
    class ANDd8 : IOpcode //AND d8
    {
        public Regex Mask => new Regex("11101000");
        public string Title => "AND";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} ${Convert.ToString(args[0], 16)}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue | value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                FlagValue.Set,
                FlagValue.Reset
            );
            inst.Registers.A.SetValue(result);
        }

    }

    class ORd8 : IOpcode //OR d8
    {
        public Regex Mask => new Regex("11110110");
        public string Title => "OR";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} ${Convert.ToString(args[0], 16)}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue | value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                FlagValue.Reset,
                FlagValue.Reset
            );
            inst.Registers.A.SetValue(result);
        }

    }
    
    class ADDd8 : IOpcode //ADD a, d8
    {
        public Regex Mask => new Regex("11000110");
        public string Title => "ADD";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} A ${Convert.ToString(args[0], 16)}";
        }

        public void Execute(Instance inst, params byte[] args)
        {

            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue + value);
            

            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);
        }

    }
    class SUBd8 : IOpcode //SUB d8
    {
        public Regex Mask => new Regex("11010110");
        public string Title => "SUB";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} ${Convert.ToString(args[0], 16)}";
        }

        public void Execute(Instance inst, params byte[] args)
        {

            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue - value);
            

            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Set,
                OpcodeUtils.IsHalfcarryFlag(recentValue, value),
                OpcodeUtils.IsCarryFlag(recentValue, value)
            );
            inst.Registers.A.SetValue(result);
        }

    }

    class XOR : IOpcode //XOR B
    {
        public Regex Mask => new Regex("10101(.{3})");
        public string Title => "XOR";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;

        public string ToString(params byte[] args)
        {
            return $"{Title} ${OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            
            var recentValue = inst.Registers.A.Value;
            var value = inst.Registers.GetRegValue(args[0]);
            var result = (byte)(recentValue ^ value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                FlagValue.Reset,
                FlagValue.Reset
            );
            inst.Registers.A.SetValue(result);
        }

    }


    
    class XORd8 : IOpcode //XOR d8
    {
        public Regex Mask => new Regex("11101110");
        public string Title => "XOR";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;

        public string ToString(params byte[] args)
        {
            return $"{Title} ${OpcodeUtils.Reg[args[0]]}";
        }

        public void Execute(Instance inst, params byte[] args)
        {

            var recentValue = inst.Registers.A.Value;
            var value = args[0];
            var result = (byte)(recentValue ^ value);


            inst.Registers.SetFlags(
                OpcodeUtils.IsNullFlag(result),
                FlagValue.Reset,
                FlagValue.Reset,
                FlagValue.Reset
            );
            inst.Registers.A.SetValue(result);
        }

    }


    class LD16A : IOpcode //LD (BC), A
    {
        public Regex Mask => new Regex("00(000|010|100|110)010");
        public string Title => "LD";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;
        public string[] Reg => new[] { "BC", ".", "DE", ".", "HC+", ".", "HC-" };

        public string ToString(params byte[] args)
        {
            return $"{Title} ({Reg[args[0]]}) A";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var regName = Reg[args[0]];
            var addr = (ushort)(inst.Registers.GetReg16Value(regName) + 1);


            if (regName == "HC+")
                inst.Registers.SetReg16Value(regName, (ushort)(inst.Registers.GetReg16Value(regName) + 1));

            if (regName == "HC-")
                inst.Registers.SetReg16Value(regName, (ushort)(inst.Registers.GetReg16Value(regName) - 1));

            inst.CopyBytesToMemory(addr, BitConverter.GetBytes(inst.Registers.GetRegValue(RegType.A)));

        }

    }

    class JR : IOpcode //JR
    {
        public Regex Mask => new Regex("00(111|101|100|110)000");
        public string Title => "JR";
        public byte LastValue { get; set; }
        public int ArgsCount => 1;
        public Dictionary<byte, string> Reg = new Dictionary<byte, string> { { 0b100, "NZ" }, { 0b101, "Z" }, { 0b110, "NC" }, { 0b111, "C"} };

        public string ToString(params byte[] args)
        {
            return $"{Title} ({Reg[args[0]]}) A";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            var flag = args[0];

            if (
                (flag == 0b100 && inst.Registers.GetFlag(Flag.Z)) || // NZ
                (flag == 0b101 && inst.Registers.GetFlag(Flag.Z)) || // Z
                (flag == 0b110 && inst.Registers.GetFlag(Flag.Z)) || // NC
                (flag == 0b111 && inst.Registers.GetFlag(Flag.Z)) // C
             )
                inst.Registers.PC += args[1];

        }

    }

    
    class LDCA : IOpcode //LD (C) A
    {
        public Regex Mask => new Regex("11100010");
        public string Title => "LDH";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;

        public string ToString(params byte[] args)
        {
            return $"{Title} (C) A";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(inst.Registers.C.Value, 0xFF), BitConverter.GetBytes(inst.Registers.A.Value));


        }

    }
    
    class LDAC : IOpcode //LD (C) A
    {
        public Regex Mask => new Regex("11100010");
        public string Title => "LDH";
        public byte LastValue { get; set; }
        public int ArgsCount => 0;

        public string ToString(params byte[] args)
        {
            return $"{Title} (C) A";
        }

        public void Execute(Instance inst, params byte[] args)
        {
            inst.CopyBytesToMemory((ushort)OpcodeUtils.GetInt(inst.Registers.C.Value, 0xFF), BitConverter.GetBytes(inst.Registers.A.Value));


        }

    }



}
