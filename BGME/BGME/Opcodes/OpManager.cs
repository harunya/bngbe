﻿using BGME.Opcodes.Default;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BGME.Opcodes
{
    public class OpManager
    {
        public static List<IOpcode> Opcodes = new List<IOpcode>();

        private Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return
              assembly.GetTypes()
                      .Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();
        }

        public OpManager()
        {
            foreach(var opcode in GetTypesInNamespace(Assembly.GetExecutingAssembly(), "BGME.Opcodes.Default")){
                Opcodes.Add((IOpcode)Activator.CreateInstance(opcode));
            }
        }
        public IOpcode GetOpcode(byte id)
        {
            var dd = Convert.ToString(id, 2).PadLeft(8, '0');
            var res = Opcodes.Find((opcode) => opcode.Mask.IsMatch(dd));
            if (res != null)
                res.LastValue = id;

            return res;
        }
    }
}
