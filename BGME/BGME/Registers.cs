﻿using BGME.Opcodes;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace BGME
{
    public enum Flag
    {
        Z = 0,
        N = 1,
        H = 2,
        C = 3
    }
    public enum FlagValue
    {
        Set = 1,
        Reset = 0,
        Notchange = -1
    }

    public enum RegType
    {
        B = 0b000,
        C = 0b001,
        D = 0b010,
        E = 0b011,
        H = 0b100,
        L = 0b101,
        HL = 0b110,
        A = 0b111
    }

    public enum Reg16Type
    {
        BC = 0b000,
        DE = 0b010,
        HLI = 0b100,
        HLD = 0b110,
        HL = 0b110,
    }

    public class Register
    {
        public byte HighNybble { get; set; }
        public byte LowNybble { get; set; }
        public byte Value { get => (byte)(HighNybble * 16 + LowNybble); }
        public string Tag { get; set; }

        public Register(string tag)
        {
            Tag = tag;
        }

        public void SetValue(byte b)
        {
            HighNybble = (byte)(b >> 4 & 0xF);
            LowNybble = (byte)(b & 0xF);
        }

        public override string ToString() => Convert.ToString(Value, 16).PadLeft(2, '0');
    }

    public class Registers
    {
        public Register A { get; set; } = new Register("A");
        public Register B { get; set; } = new Register("B");
        public Register D { get; set; } = new Register("D");
        public Register H { get; set; } = new Register("H");


        public Register F { get; set; } = new Register("F");
        public Register C { get; set; } = new Register("C");
        public Register E { get; set; } = new Register("E");
        public Register L { get; set; } = new Register("L");

        public int PC { get; set; } = 0;


        public byte GetRegValue(short id) => GetRegValue((RegType)id);
        public byte GetRegValue(RegType b)
        {
            var s = Enum.GetName(typeof(RegType), b);
            var r = GetType().GetProperty(s).GetValue(this);
            return ((Register)r).Value;
        }
        public void SetRegValue(short id, byte value) => SetRegValue((RegType)id, value);
        public void SetRegValue(RegType b, byte value)
        {
            var s = Enum.GetName(typeof(RegType), b);
            var r = GetType().GetProperty(s).GetValue(this);
            var tmp = ((Register)r);
            tmp.SetValue(value);

            // TODO: Is may be not work
        }

        public void SetReg16Value(string name, ushort value)
        {
            var bytes = BitConverter.GetBytes(value);

            switch (name.ToUpper())
            {
                case "BC":
                    B.SetValue(bytes[0]);
                    C.SetValue(bytes[1]);
                    break;
                case "DE":
                    D.SetValue(bytes[0]);
                    E.SetValue(bytes[1]);
                    break;
                case "HL+":
                case "HL-":
                case "HL":
                    H.SetValue(bytes[0]);
                    L.SetValue(bytes[1]);
                    break;
            }

        }
        public ushort GetReg16Value(string name)
        {
            switch (name.ToUpper())
            {
                case "BC":
                    return (ushort)OpcodeUtils.GetInt(B.Value, C.Value);
                case "DE":
                    return (ushort)OpcodeUtils.GetInt(D.Value, E.Value);
                case "HL+":
                case "HL-":
                case "HL":
                    return (ushort)OpcodeUtils.GetInt(H.Value, L.Value);
            }

            return 0;
        }

        public bool GetFlag(Flag f)
        {
            return OpcodeUtils.GetBit(F.Value, (int)f);
        }

        public void SetFlag(Flag f, bool value)
        {
            int pos = (int)f;
            byte aByte = F.Value;

            F.SetValue((byte) ((value) ? (aByte | (1 << pos)) : (aByte & ~(1 << pos))));
        }
        public void SetFlagOrNot(Flag f, FlagValue value)
        {
            if ((short)value != -1)
                SetFlag(f, (short)value == 1 ? true : false);
        }
        public void SetFlags(FlagValue z, FlagValue n, FlagValue h, FlagValue c)
        {
            SetFlagOrNot(Flag.Z, z);
            SetFlagOrNot(Flag.N, n);
            SetFlagOrNot(Flag.H, h);
            SetFlagOrNot(Flag.C, c);
        }

    }
}
