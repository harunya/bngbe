﻿using BGME.Data;
using BGME.Opcodes;
using Microsoft.VisualBasic.CompilerServices;
using Mindmagma.Curses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Threading;

namespace BGME
{
    class Emu : Instance
    {
        public unsafe void Run(string path, Action<string> log)
        {
            var bytes = File.ReadAllBytes(path);

            Data = GameData.LoadFromBytes(bytes);

            for (Registers.PC = GameData.EntryPointAddress; Registers.PC < Data.Instructions.Length;)
            {
                var b = Data.Instructions[Registers.PC];
                string buffer ="";

                buffer += $"\r${Registers.PC:X8} [0x{b:X2}]: ";

                if (b == 0xCB)
                { // Prefix
                    Registers.PC++;
                    log(buffer);
                    break;
                }

                

                var opcode = OpManager.GetOpcode(b);
                if (opcode == null)
                {
                    Registers.PC++;
                    buffer += $"Can't recognize";
                    log(buffer);
                    continue;
                }

                byte[] opargs = new byte[opcode.ArgsCount];
                Array.Copy(Data.Instructions, Registers.PC+1, opargs, 0, opcode.ArgsCount);

                Registers.PC += opcode.ArgsCount+1;
                opcode.Eval(this, opargs);

                buffer += opcode.GetOpDesc(opargs);

                log(buffer);
                
            }
        }
    }

    class Program
    {
        public static Emu emuInstance;
        unsafe static void Main(string[] args)
        {
            emuInstance = new Emu();

            int maxSize = 0;
            var strs = new List<string>();
            emuInstance.Run("tetris.gb", (s)=> {
                Console.WriteLine(s);
            });


        }
    }
}
