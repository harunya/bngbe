﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BGME.Data
{
    public static class GameDataSize
    {
        public static int EntryPoint = 4;
        public static int NintendoGraphic = 48;
        public static int Title = 16;
        public static int ColorGB = 1;
        public static int TypeFunctions = 1;
        public static int CartridgeType = 1;
        public static int RomSize = 1;
        public static int RamSize = 1;
        public static int DestinationCode = 1;
        public static int LicenseCode = 1;
        public static int RomVerstionMask = 1;
        public static int ComplementCheck = 1;
        public static int Cheksum = 2;
    }
    public unsafe class GameData
    {
        public static int EntryPointAddress { get; } = 0x150;

        public byte[] EntryPoint { get; private set; }
        public byte[] NintendoGraphic { get; private set; }
        public string Title { get; private set; }
        public byte ColorGB { get; private set; }
        public byte TypeFunctions { get; private set; }
        public byte CartridgeType { get; private set; }
        public byte RomSize { get; private set; }
        public byte RamSize { get; private set; }
        public byte DestinationCode { get; private set; }
        public byte LicenseCode { get; private set; }
        public byte RomVerstionMask { get; private set; }
        public byte ComplementCheck { get; private set; }
        public byte[] Cheksum { get; private set; }
        public byte[] Instructions { get; private set; }


        private GameData(byte[] array)
        {
            EntryPoint = array.Skip(0x100).Take(3).ToArray();
            NintendoGraphic = array.Skip(0x104).Take(48).ToArray();
            Title = Encoding.ASCII.GetString(array.Skip(0x0134).Take(16).ToArray(), 0, 16).Replace("\0", "").Replace("?", "");
            ColorGB = array[0x0143];

            Instructions = array.ToArray();


        }
        public static GameData LoadFromBytes(byte[] array)
        {
            return new GameData(array);
        }

        public void CopyBytesToMemory(ushort addr, byte[] array)
        {
            for(int i=0; i<array.Length; i++)
                Instructions[addr + i - 0xC000] = array[i];
        }
    }
}
