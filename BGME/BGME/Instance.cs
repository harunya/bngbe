﻿using BGME.Data;
using BGME.Opcodes;
using System;
using System.Collections.Generic;
using System.Text;

namespace BGME
{
    public class Instance
    {
        public Registers Registers { get; set; } = new Registers();
        public OpManager OpManager { get; set; } = new OpManager();
        public GameData Data { get; set; }

        public Register[] GetAllRegisters()
        {
            var r = new List<Register>();

            foreach (var reg in Registers.GetType().GetProperties()) 
            {
                if (reg.PropertyType == typeof(Register))
                    r.Add((Register)reg.GetValue(Registers));
            }

            return r.ToArray();
        }
        public byte[] RAM { get; set; } = new byte[16 * 1024];

        public void CopyBytesToMemory(ushort addr, byte[] array)
        {
            addr -= 0xC000;
            if (addr < 0 || addr + array.Length > RAM.Length)
                return;

            for (int i = 0; i < array.Length; i++)
                RAM[addr + i] = array[i];
        }
        public byte[] GetBytesFromMemory(ushort addr, ushort count)
        {
            addr -= 0xC000;
            if (addr < 0 || addr + count > RAM.Length)
                return new byte[count];

            byte[] res = new byte[count];
            Array.Copy(RAM, addr, res, 0, count);

            return res;
        }
    }
}
